TEMPLATE = lib
TARGET = durability
CONFIG += plugin debug

DEFINES += __DURABILITY_VERSION__=\\\"1.0.0\\\"

DEPENDPATH += ../maintask
LIBS += ../maintask/libmaintask.a
LIBS += ../pugixml/libpugixml.a
# Input
HEADERS += framework.hpp
#           ../maintask/calc_class.hpp\
#           ../maintask/constants_holder.hpp

SOURCES += framework.cpp
           #../calc_class.cpp
