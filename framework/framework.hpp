#ifndef __FRAMEWORK__FRAMEWORK_HPP__
#  define __FRAMEWORK__FRAMEWORK_HPP__

#  include <QObject>
#  include <QString>
#  include "../maintask/calc_class.hpp"
#  include "../maintask/logger.hpp"

class durability_framework : public QObject
{
Q_OBJECT
    calc_class m_calc;
    logger log;

    QString constholder_as_string(const constants_holder<double>& v) const;
public:
    durability_framework() :  m_calc()
    {}

    bool work(QString&);
    bool calc_diagram(QString&);

    void requestVersion()
    {
        QString versionInfo(QString::fromLocal8Bit("Durability library: "));
        ///\todo think how can i build in version info without having this define outside?
        //versionInfo.append(QString::fromLocal8Bit(__DURABILITY_VERSION_));
        versionInfo.append("\nBuild date: ");
        versionInfo.append(__DATE__);
        versionInfo.append("\nBuild time:");
        versionInfo.append(__TIME__);
        emit sendVersion(versionInfo);
    }

signals:
    void finished(int, const QString&);
    void finishedDiagram(int, const QString&);
    void errorDurability(int code_, const QString&);
    void warningDurability(int code_, const QString&);

    void sendVersion(const QString&);
};

#endif // __FRAMEWORK__FRAMEWORK_HPP__
