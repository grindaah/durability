#ifndef __D_EXCEPTIONS_HPP__
#  define __D_EXCEPTIONS_HPP__

#include <exception>
#include <string>
#include <unistd.h>
#include <errno.h>

#include "../pugixml/pugixml.hpp"

class d_exception
{

public:
    enum exception_type
    {
        standart_exception = 1
      , xml_exception
      , invalid_input_data
      , interpolate_error_Fr
      , interpolate_error_Lambda
      , invalid_constant_access
    };

    int code;
    int xml_code;
    std::string description;
    std::string addition;

    ///constructors
    d_exception(const pugi::xml_parse_result& result
              , const std::string& full_path);
    d_exception(int given_code);
    d_exception(std::exception ex);

    std::string what();
};

#endif // __D_EXCEPTIONS_HPP__
