#ifndef __EXTERNAL_FORCES_HPP__
#  define __EXTERNAL_FORCES_HPP__

#  include <vector>

struct external_forces
{
    double M_h2_vv;
    double M_h2_pv;

    double M_vd;
    double M_pd;
};

struct impulse_external_forces : public external_forces
{
    double M_h1_vv;
    double M_h1_pv;
};

#endif // __EXTERNAL_FORCES_HPP__
