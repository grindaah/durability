#include "logger.hpp"

logger::logger(log_level_t l)
{
    m_loglevel = l;
}

logger::~logger()
{
    std::cout << std::endl;
}

log_level_t logger::reporting_level()
{
    return m_loglevel;
}

void logger::set_level(log_level_t l)
{
    m_loglevel = l;
}

const char* logger::leveltostring( log_level_t level )
{
    return buffer[level];
}

log_level_t logger::levelfromstring(const char* ch_level)
{
    std::string cmp_str = ch_level;

    if (cmp_str == "debug")
        return log_debug;
    if (cmp_str == "info")
        return log_info;
    if (cmp_str == "trace")
        return log_trace;
    if (cmp_str == "warning")
        return log_warning;
    if (cmp_str == "error")
        return log_error;

    //logger().get_msg(log_warning) << "Unknown logging level '" << ch_level << "'. Using INFO level as default.";
    return log_info;
}

/*std::ostringstream& logger::get_msg(log_level_t level)
{
    if (level > log_debug);
    else if(level > logger::reporting_level());
    else
    {
        return std::cout << "(" << leveltostring(level) << "):";
    }
    return std::stringstream();
}*/
