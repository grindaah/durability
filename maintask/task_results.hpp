#ifndef __TASK_RESULTS_HPP__
#  define __TASK_RESULTS_HPP__

#  include "compare.hpp"

#  include <QString>

#  include <vector>

struct task_results
{
    double n_m_zp_min;
    double n_q_zp_min;
    std::vector<double> M_sum_max;
    std::vector<double> Q_sum_max;

    std::vector<double> n_m_zp_min_all;

    task_results():
        n_m_zp_min(0.)
      , n_q_zp_min(0.)
    {
        M_sum_max.clear();
        Q_sum_max.clear();
        n_m_zp_min_all.clear();
    }

    void check_n_m_min(double n_m)
    {
        n_m_zp_min = cmpr::is_zero(n_m_zp_min) ?
                    n_m :
                    std::min(n_m_zp_min, n_m);
    }

    void check_n_q_min(double n_q)
    {
        n_q_zp_min = cmpr::is_zero(n_q_zp_min) ?
                    n_q :
                    std::min(n_q_zp_min, n_q);
    }

    QString give_diag_results(const int V, const int step)
    {
        std::stringstream ss;
        QString result;

        ss.precision(2);
        ss.setf(std::ios_base::fixed);

        ss << "V" << V << "_s" << step << "=[";
        for (size_t i = 0; i < n_m_zp_min_all.size(); ++i)
        {
            if (i !=  n_m_zp_min_all.size() - 1)
                ss << n_m_zp_min_all[i] << ", ";
            else
                ss << n_m_zp_min_all[i];
        }

        ss << "];";

        result.append(QString(ss.str().c_str()));
        return result;
    }

    QString give_results()
    {
        std::stringstream ss;

        ss.precision(2);
        ss.setf(std::ios_base::fixed);

        ss << "stock_durability=[";
           //<< n_m_zp_min
        for (size_t i = 0; i < n_m_zp_min_all.size(); ++i)
        {
            if (i !=  n_m_zp_min_all.size() - 1)
                ss << n_m_zp_min_all[i] << ", ";
            else
                ss << n_m_zp_min_all[i];
        }

        ss << "]; n_q_zp_min="
           << n_q_zp_min;

        //std::cerr << "\n| debug: " << ss.str() << " |";

        ss << "; M_sum_max=[";
        for (size_t i = 0; i < M_sum_max.size(); ++i)
        {
            if (i < M_sum_max.size()-1)
                ss << M_sum_max[i] << ", ";
            else
                ss << M_sum_max[i];
        }

        ss << "]; Q_sum_max=[";

        for (size_t i = 0; i < Q_sum_max.size(); ++i)
        {
            if (i < Q_sum_max.size()-1)
                ss << Q_sum_max[i] << ", ";
            else
                ss << Q_sum_max[i];
        }
        ss << "];";

        //std::cerr << "\n| debug2: " << ss.str() << " |\n";

        //for (size_t i = 0; i < n_m_zp_min_all.size(); ++i)
        //    ss << "n_m_zp_min" << i+1 << "=" << n_m_zp_min_all[i] << ";";

        QString result(ss.str().c_str());
        return result;
    }

    void flush()
    {
        M_sum_max.clear();
        Q_sum_max.clear();
        n_m_zp_min_all.clear();
        //M_sum_max.resize(0);
        //Q_sum_max.clear();
        n_m_zp_min_all.resize(0);
        n_m_zp_min = 0.;
        n_q_zp_min = 0.;
    }
};

#endif // __TASK_RESULTS_HPP__
