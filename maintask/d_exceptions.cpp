#include "d_exceptions.hpp"
#include <unistd.h>
#include <sstream>

d_exception::d_exception(const pugi::xml_parse_result& result
          , const std::string& full_path)

    : code(xml_exception)
    , xml_code(result.status)
    , description(result.description())
    , addition(full_path)
{}

d_exception::d_exception(int given_code)

    : code(given_code)
    , xml_code(0)
{}

d_exception::d_exception(std::exception ex)
    : code(standart_exception)
    , xml_code(0)
    , description(ex.what())
{}

std::string d_exception::what()
{
    switch (code)
    {
    case standart_exception:
        return description;
    break;
    case xml_exception:
    {
        if (xml_code)
        {
            std::stringstream ss;
            ss << "pugi xml reporeted error: "
               << description
               << "working directory is:"
               << addition;
            return ss.str();
        }
        else
        {
            return "unknown xml error";
        }
    }
    break;
    case interpolate_error_Fr:
        return "cannot interpolate kd, sigma value not inside interval!";
    break;
    case invalid_input_data:
        return "Invalid input data";
    break;
    case interpolate_error_Lambda:
        return  "cannot interpolate lambda_dash_1, sigma_0 not inside interval!";
    break;
    case invalid_constant_access:
    {
        std::stringstream ss;
        ss << "invalid access to constants element: " << addition;
        return  ss.str();
    }
    break;

    default:
        return "exception with unknown code";
    break;
    };

    return "exception with unknown code, unsuccesfully handled";
}
