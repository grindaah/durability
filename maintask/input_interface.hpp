#ifndef __INPUT_INTERFACE_HPP__
#  define __INPUT_INTERFACE_HPP__

#  include <QString>
#  include <sstream>
#  include <vector>
#  include <set>

#  include "string_trimming.hpp"
#  include "d_exceptions.hpp"
#  include "compare.hpp"
#  include "constants_holder.hpp"

struct input_interface
{
    struct var
    {
        var() :
            value(-1.)
        {
        }

        std::string name;
        double value;
    };

    input_interface()
    {
    }

    std::vector<std::string> &split(const std::string &s
                                  , char delim
                                  , std::vector<std::string> &elems
                                  )
    {
        std::stringstream ss(s);
        std::string item;
        while (std::getline(ss, item, delim))
        {
            elems.push_back(item);
        }
        return elems;
    }

    std::vector<std::string> split(const std::string &s, char delim)
    {
        std::vector<std::string> elems;
        split(s, delim, elems);
        return elems;
    }

    void parse_elems(const std::string& s)
    {
        //trim brackets
        std::string& m_string = const_cast<std::string&>(s);
        m_string.erase(m_string.begin()
                       , m_string.begin() + m_string.find_first_of('[', 0)+1);
        m_string.erase(m_string.end() - 1);
        std::stringstream ss(s);
        std::string item;

        while (std::getline(ss, item, ','))
        {
            trim(item);
            excluded_elements.insert(item);
            std::cout << "element append as excluded:" << item << std::endl;
        }
    }

    void parse_explosion(const std::string& s)
    {
        //trim brackets
        std::string& m_string = const_cast<std::string&>(s);
        m_string.erase(m_string.begin()
                       , m_string.begin() + m_string.find_first_of('[', 0)+1);
        m_string.erase(m_string.end() - 1);
        std::stringstream ss(s);
        std::string item;

        while (std::getline(ss, item, ','))
        {
            trim(item);
            explosion_sphere >> item.c_str();
            std::cout << "Explosion parameter added: " << item << std::endl;
        }
        if (explosion_sphere.size() >0 && explosion_sphere.size() < 4)
        {
            d_exception ex(d_exception::invalid_input_data);
            ex.description = "Wrong inputs: Explosion";
            throw ex;
        }
    }

    ///\todo stringstream is very slow solution here!
    var split_in_two(std::string val_pair)
    {
        var v;
        std::stringstream ss(val_pair);
        std::getline(ss, v.name, '=');
        std::cout << "splitting: " << v.name << std::endl;
        if (ss.str().size()
         && v.name == "Elems"
         && ss.str()[ss.str().length()-1] == ']'
         )
            parse_elems(ss.str());
        else if (ss.str().size()
              && v.name == "Explosion"
              && ss.str()[ss.str().length()-1] == ']')
            parse_explosion(ss.str());
        else
            ss >> v.value;
        return v;
    }

    void get_inputs(const QString& str)
    {
        flush();
        std::vector<std::string> variable_strings;
        variable_strings = split(str.toStdString(), ';');

        for (size_t i = 0; i < variable_strings.size(); ++i)
        {
            var v;
            v = split_in_two(variable_strings[i]);
            if ((v.name != "Elems") && v.name != "Explosion")
            {
                if (cmpr::equal(-1.0, v.value))
                {
                    d_exception ex(d_exception::invalid_input_data);
                    ex.description = "invalid input value: " + v.name;
                    throw ex;
                }
                input_vars.append_element(v.name, v.value);
            }
            std::cout << "inputs append: " << v.name
                      << "=" << v.value << std::endl;
        }
    }

    var_base input_vars;
    std::set<std::string> excluded_elements;
    constants_holder<double> explosion_sphere;

private:
    void flush()
    {
        excluded_elements.clear();
    }
};

#endif // __INPUT_INTERFACE_HPP__
