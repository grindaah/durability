#ifndef __GRAPH_2D_HPP__
#  define __GRAPH_2D_HPP__

#  include <vector>

template <typename T>
class graph_2d
{
    std::vector<T> x;
    std::vector<T> y;

    graph_2d(const std::vector<T> x1
           , const std::vector<T> y1
           ) :
        x(x1)
      , y(y1)
    {
    }
};


#endif // __GRAPH_2D_HPP__
