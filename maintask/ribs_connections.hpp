#ifndef __RIBS_CONNECTIONS_HPP__
#  define __RIBS_CONNECTIONS_HPP__

#  include "../pugixml/pugixml.hpp"
#  include "ribframe.hpp"

#  include "constants_holder.hpp"
#  include "d_exceptions.hpp"
#  include "compare.hpp"

#  include <QString>
#  include <vector>
#  include <set>

/**
 * \class ribs_holder
 * \brief class for holding and perform calculations over
 *        ribs data
 *
 */
class ribs_holder
{
    void set_M_tv();

public:

    ribs_holder()
    {}
    ribs_holder(std::vector<rib_frame_inputs> rez);

    std::vector<rib_frame_inputs> frame_inputs;
    std::vector<double> M_tv;                              ///< Момент на тихой воде
    std::vector<double> Q_tv;                              ///< Перерезывающие силы на тихой воде
    std::vector<double> t_sum;                             ///< Суммарная толщина бортов
    std::vector<double> X;                                 ///< X-координаты шпангоута
    std::vector<double> X_zone;
    std::vector<double> delta_Z;                           ///< Смещение к оси Z 3D модели

    std::vector<ribframe> frames;
    var_base var;

    void calc_rib(int index);

    ///returns list of exploded elements in QString
    QString exclude(const std::set<std::string>& ids);
    ///returns list of excluded elements in QString
    QString explode(const std::vector<sphere>& explosions);
    std::set<std::string> calc_explosion(const sphere&);
    void update(std::vector<rib_frame_inputs> rez);

    void exclude_frame(size_t frame_no, std::set<std::string>& ids);
    void explode_frame(size_t frame_no, const std::set<std::string>& ids);
    void imbue_rib_data(const pugi::xml_document& doc, ribframe& rib);
    void xml_exception_thrower(const pugi::xml_parse_result& result);
};

#endif // RIBS_CONNECTIONS_HPP
