QT       += core

QT       -= gui

TEMPLATE = subdirs
CONFIG += ordered debug

QMAKE_CXXFLAGS_DEBUG += -g

SUBDIRS = pugixml\
          maintask\
          framework\
          app

maintask.depends = pugixml
framework.depends = maintask
app.depends = framework

